export const environment = {
  production: true,
  LOGIN_PWD: "https://shopeeholic-server.herokuapp.com/users/login/pwd",
  GET_CURRENT_USER: "https://shopeeholic-server.herokuapp.com/users/me/"
};