export const environment = {
  production: false,
  APP_ROOT: "https://shopeeholic-server.herokuapp.com",
  LOGIN_PWD: "https://shopeeholic-server.herokuapp.com/users/login/pwd",
  GET_CURRENT_USER: "https://shopeeholic-server.herokuapp.com/users/me/",
  GET_PRODUCTS: "https://shopeeholic-server.herokuapp.com/products/of/saleman/",
  GET_PRODUCT: "https://shopeeholic-server.herokuapp.com/products/",
  UPLOAD_PRODUCT_MEDIA: "https://shopeeholic-server.herokuapp.com/products/saleman/prod/media/",
  UPLOAD_USER_MEDIA: "https://shopeeholic-server.herokuapp.com/users/media"
};
